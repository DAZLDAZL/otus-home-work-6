﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoInitializer : IDbInitializer
    {
        private readonly MongoContext _dataContext;

        public MongoInitializer(MongoContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            _dataContext.GetCollection<Employee>(nameof(Employee)).DeleteMany(x => true);
            _dataContext.GetCollection<Employee>(nameof(Employee)).InsertMany(FakeDataFactory.Employees);
        }
    }
}
