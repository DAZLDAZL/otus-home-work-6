﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.Administration.DataAccess
{
    public class MongoContext
    {
        private readonly IMongoDatabase database = null;

        public MongoContext(IConfiguration config)
        {
            var client = new MongoClient(config.GetConnectionString("MongoPromoCodeFactoryAdministrationDb"));
            if( client != null)
            {
                database = client.GetDatabase("PromoCodeFactoryAdministration");
            }
        }

        public IMongoCollection<T> GetCollection<T>(string name) => database.GetCollection<T>(name);
    }
}
