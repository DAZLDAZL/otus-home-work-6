﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly IMongoCollection<T> collection;
        

        public MongoRepository(MongoContext _context)
        {
            collection = _context.GetCollection<T>(typeof(T).Name);
        }

        public async Task AddAsync(T entity)
        {
            await collection.InsertOneAsync(entity);
        }

        public async Task DeleteAsync(T entity)
        {
            await collection.DeleteOneAsync( x => x.Id == entity.Id );
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await (await collection.FindAsync(x => true)).ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var res = await collection.FindAsync(x => x.Id == id);
            if(res == null)
                throw new Exception("Not Found");
            return await res.FirstOrDefaultAsync();
        }

        public async Task<T> GetFirstWhere(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            var res = await collection.FindAsync(predicate);
            if (res == null)
                throw new Exception("Not Found");
            return await res.FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var res = await collection.FindAsync(x => ids.Contains(x.Id));
            return await res.ToListAsync();
        }

        public async Task<IEnumerable<T>> GetWhere(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            var res = await collection.FindAsync(predicate);
            return await res.ToListAsync();
        }

        public async Task UpdateAsync(T entity)
        {
           await collection.ReplaceOneAsync(x => x.Id == entity.Id, entity);
        }
    }
}
